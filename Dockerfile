# Pull base image
FROM python:2

MAINTAINER Talmai Oliveira <talmai.oliveira@philips.com>

RUN pip install crate
RUN pip install crate_riot

# create directory for startup scripts
CMD mkdir /riot/
COPY run_node.sh /riot/

RUN chmod +x /riot/run_node.sh
ENTRYPOINT ["/riot/run_node.sh"]


#CMD [ "python", "./my_script.py" ]
#CMD python