# README #

Container responsible for running a python script to querry data from crate and output a data file.

### What is this repository for? ###
It is assumed that user has logged in to docker using the lightingiotdevices user which has been configured for readonly access to 
the devices team on the private dockerhub account, and to a select subset of images (specifically in this case, it will have access 
to the lightingiot/rpi-riot-node image)

$ docker login --username=lightingiotdevices --password=Docker@2017

### how to write python model and publish to PyPI and make it pipable###

your file structure should be like:

```
#!python


funniest/

    funniest/

        __init__.py

    setup.py
```

	
setup.py should be like following:


```
#!python

from setuptools import setup
setup(name='funniest',
      version='0.1',
      description='The funniest joke in the world',
      url='http://github.com/blaaa',
      author='someone',
      author_email='someone@example.com',
      license='yourcompany',
      packages=['funniest'],
      zip_safe=False)
```

	 
install the package locally
```
#!python
$ pip install .
	 
Publishing On PyPI, to update metadata and publish a new build in a single step:

$ python setup.py register sdist upload

Finally, install it using

$ pip install crate_riot
```
### how to build images and run ###
```
#!python

$ cd tutorial_docker_crate_python

$ docker build -t crate1 .

$ cd your_directory_with_python.py
```

```
#!python

$ docker run --rm --name my-running-script -v "$PWD":/usr/src/crate_read -w /usr/src/crate_read   crate1:latest exam1.py 
```


* "$PWD":/usr/src/crate_read set your local working directionary with the path in container,so your py file could be visit within docker container
* -w /usr/src/crate_read set the current working directionary is /usr/src/crate_read in container where the dependency python file is (main.py and crate_read.py)


### crate database structure ###
table name:riot

column name

1. adc_zero_raw (NULL,"0E")

2. host (riot)

3. lux_input_ambient_ir (Null,"3F00")

4. lux_input_ir(Null,"0B00")

5. node_id (riot001,riot025,CBGDemo)
 
6. path (/api/robots/riotBot/devices/riot/commands/ReadDigitalInput)

7. port (8080)
 
8. result ({"result{"digitalInput01":"0","digitalInput02":"1","digitalInput03":"1","digitalInput04":"1","err[null],"raw":"0E"}})

9. sensor_type(digitalio,thermopile,lux)

10. thermopile_input_local(NULL,"0B68")

11. thermopile_input_object(NULL,"0F6C")
 
12. timestamp(1486756444608)

port is integer and others are all string


=======

### Who do I talk to? ###

Talmai Oliveira

Jingyi Guo