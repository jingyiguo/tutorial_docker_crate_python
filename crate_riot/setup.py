from setuptools import setup

setup(name='crate_riot',
      version='0.1',
      description='Getting data from crate server',
      url='https://bitbucket.org/research_iot/tutorial_docker_crate_python',
      author='Jingyi Guo',
      author_email='plips4847@gmail.com',
      license='Phil',
      packages=['crate_riot'],
      zip_safe=False)
